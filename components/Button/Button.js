import React from 'react'

const Button = ({ children }) => (
  <button className="button is-block is-info is-large is-fullwidth">{children}</button>
)

export default Button